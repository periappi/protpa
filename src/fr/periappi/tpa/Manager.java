package fr.periappi.tpa;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Manager {

    Main plugin;

    public Manager(Main plugin) {
        this.plugin = plugin;
    }

    public void sendRequest(String sender, String target){
        plugin.requests.put(sender, target);

        if(plugin.counter.containsKey(sender)){
            int count = plugin.counter.get(sender) + 1;
            plugin.counter.put(sender, (count >= plugin.daily_limit ? count : count - 1));
        }else{
            plugin.counter.put(sender, 1);
        }

        Bukkit.getScheduler().runTaskLater(plugin, new Runnable() {
            @Override
            public void run() {
                if(isPlayerConnected(sender) && plugin.requests.containsKey(sender)) {
                    plugin.getServer().getPlayer(sender).sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED  + plugin.requests.get(sender) + " hasn't answered your request.");
                }
                removeRequest(sender);
            }
        }, plugin.request_cooldown * 20);
    }

    public void removeRequest(String sender){
        if(hasRequest(sender)){
            plugin.requests.remove(sender);
        }
    }

    public boolean hasRequest(String sender){
        if(plugin.requests.containsKey(sender)) return true;
        return false;
    }

    public boolean isPlayerConnected(String player){
        Player p = plugin.getServer().getPlayerExact(player);
        if(plugin.getServer().getOnlinePlayers().contains(p)) return true;
        return false;
    }

    public Integer getCount(String player){
        if(plugin.counter.containsKey(player)) return plugin.counter.get(player);
        return 0;
    }

    public void resetCounters(){
        plugin.counter.clear();
    }

    public boolean canRequest(String player){
        if(!plugin.counter.containsKey(player)){
            return true;
        }
        else if(plugin.counter.get(player) >= plugin.daily_limit) {
            return false;
        }
        return true;
    }
}
