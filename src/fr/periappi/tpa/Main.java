package fr.periappi.tpa;

import net.milkbowl.vault.permission.Permission;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;

public class Main extends JavaPlugin{

    //TODO Permission accepter refuser tpa

    Manager m;

    public Permission permission = null;

    public String serveur_name;
    public String website_url;
    public String permission_name;
    public String unlimited_tpa_permission_name;
    public int reset_hour;
    public int daily_limit;
    public int request_cooldown;

    HashMap<String, Integer> counter = new HashMap<>();
    HashMap<String, String> requests = new HashMap<>();

    @Override
    public void onDisable() {}

    @Override
    public void onEnable() {
        getLogger().info("Loading configuration...");
        setupConfig();
        m = new Manager(this);
        setupPermissions();
        getCommand("tpa").setExecutor(new TPACommands(this));
        scheduleTasksReset();
        getLogger().info("Loaded!");
    }

    private void setupConfig(){
        if(!getDataFolder().exists()){
            getDataFolder().mkdir();
        }
        File configFile = new File(getDataFolder() + File.separator + "config.yml");
        FileConfiguration config = YamlConfiguration.loadConfiguration(configFile);
        if(!configFile.exists()){
            config.set("serveur_name", "Minecraft Server");
            config.set("website_url", "http://spigotmc.org/");
            config.set("permission_name", "myserver.tpa");
            config.set("unlimited_tpa_permission_name", "myserveur.tpa.*");
            config.set("reset_hour", 0);
            config.set("daily_limit", 5);
            config.set("request_cooldown", 60);
            try {
                config.save(configFile);
            } catch (IOException e) {
                e.printStackTrace();
                getLogger().log(Level.WARNING, "Cannot save config.yml!");
            }
        }
        serveur_name = config.getString("serveur_name");
        website_url = config.getString("website_url");
        permission_name = config.getString("permission_name");
        unlimited_tpa_permission_name = config.getString("unlimited_tpa_permission_name");
        reset_hour = config.getInt("reset_hour");
        daily_limit = config.getInt("daily_limit");
        request_cooldown = config.getInt("request_cooldown");
    }

    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            permission = permissionProvider.getProvider();
        }
        return (permission != null);
    }

    private void scheduleTasksReset() {
        getServer().getScheduler().scheduleAsyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                Date date = new Date();
                if((date.getHours() == 0 && date.getMinutes() == 0)){
                    m.resetCounters();
                }
            }
        }, 0, 30 * 20);//Each 30 seconds
    }
}
