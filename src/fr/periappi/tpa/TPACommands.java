package fr.periappi.tpa;

import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.logging.Level;

public class TPACommands implements CommandExecutor{

    Main plugin;

    public TPACommands(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if(!(sender instanceof Player)){
            plugin.getLogger().log(Level.WARNING, "Only in game users are allowed to use that command!");
        }

        Player p = (Player)sender;

        // /tpa <joueur>| <accept>/<deny> <joueur>
        if(plugin.permission.has(p, plugin.permission_name) || p.isOp()) {
            if(args.length == 0){
                if(p.isOp() || plugin.permission.has(p, plugin.unlimited_tpa_permission_name)){
                    p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.WHITE + "Your request's balance is unlimited!");
                }else{
                    int remaining = plugin.daily_limit - plugin.m.getCount(p.getName());
                    p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.WHITE + "You have " + ChatColor.RED + remaining + "/" + plugin.daily_limit + ChatColor.WHITE + " requests remaining");
                }
            }
            else if (args.length == 1){
                if(plugin.m.canRequest(p.getName()) || p.isOp() || plugin.permission.has(p, plugin.unlimited_tpa_permission_name)){
                    String playerString = args[0];
                    if(plugin.m.isPlayerConnected(playerString)){
                        Player player = plugin.getServer().getPlayer(playerString);
                        if(!plugin.requests.containsKey(p.getName())){
                            if(!player.getName().equalsIgnoreCase(p.getName())) {
                                plugin.m.sendRequest(p.getName(), player.getName());//Envoie de la requête
                                p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.YELLOW + "A request has been sent to " + player.getName());
                                FancyMessage msg1 = new FancyMessage()
                                        .text(p.getName() + " wants to teleport on you, do you agree?\n").color(ChatColor.YELLOW)
                                        .then("[ACCEPT]").color(ChatColor.GREEN)
                                        .command("/tpa accept " + p.getName())
                                        .tooltip(ChatColor.GRAY + "Accept that " + p.getName() + " teleports on you.")
                                        .then(" || ")
                                        .then("[REFUSE]").color(ChatColor.RED)
                                        .command("/tpa deny " + p.getName())
                                        .tooltip(ChatColor.GRAY + "Refuse that " + p.getName() + " teleports on you.");
                                msg1.send(plugin.getServer().getPlayer(player.getName()));
                            }else{
                                p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + "Operation cancelled! You cannot invite yourself...");
                            }
                        }else{
                            p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + "You still have a pending request, please wait " + plugin.request_cooldown + " seconds.");
                        }
                    }else{
                        p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + playerString + " doesn't exist or isn't online.");
                    }
                }else{
                    p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + "Your are limited to " + plugin.daily_limit + (plugin.daily_limit > 1 ? " requests " : " request ") + " ; you have to wait until the next " + plugin.reset_hour + ":00.");
                }
            }
        }else{
            FancyMessage msg = new FancyMessage()
                    .text("[ProTPA] ").color(ChatColor.GOLD)
                    .then("You doesn't have the permission to use the TPA system.").color(ChatColor.RED)
                    .then("Go get details on our website !").color(ChatColor.GREEN)
                    .tooltip(ChatColor.YELLOW + "Redirect you on the website of " + plugin.serveur_name)
                    .link(plugin.website_url);
            msg.send(p);
        }
             if(args.length == 2){
                String player = args[1];
                if (args[0].equalsIgnoreCase("accept")) {
                    if (plugin.m.isPlayerConnected(player)) {
                        if (plugin.requests.containsValue(p.getName()) && plugin.requests.get(player).equals(p.getName())) {
                            plugin.m.removeRequest(player);
                            plugin.getServer().getPlayer(player).teleport(p);
                            p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.YELLOW + "You just accepted the request of " + player + ".");
                            plugin.getServer().getPlayer(player).sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.YELLOW + p.getName() + " accepted your request. Teleportation...");
                        } else {
                            p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + player + " didn't sent you a request.");
                        }
                    } else {
                        p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + player + " doesn't exist or isn't online.");
                    }
                }
                else if (args[0].equalsIgnoreCase("deny")) {
                    if (plugin.m.isPlayerConnected(player)) {
                        if (plugin.requests.containsValue(p.getName()) && plugin.requests.get(player).equals(p.getName())) {
                            plugin.m.removeRequest(player);
                            plugin.getServer().getPlayer(player).sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + p.getName() + " refused your request.");
                            p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + "You just refused the request of " + player + ".");
                        } else {
                            p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + player + " didn't sent you a request.");
                        }
                    } else {
                        p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + player + " doesn't exist or isn't online.");
                    }
                }
                else {
                    p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + "Syntax error.");
                    return false;
                }
            }

            else{
                p.sendMessage(ChatColor.GOLD + "[ProTPA] " + ChatColor.RED + "Syntax error.");
                return false;
            }


        return true;
    }
}
